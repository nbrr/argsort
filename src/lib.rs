pub fn argsort(v: &[i64]) -> Vec<usize> {
    fn quicksort(v: &[i64], mut perm: &mut [usize], left: usize, right: usize) {
        if left < right {
            let p = partition(v, &mut perm, left, right);
            if p > 0 {
                quicksort(v, &mut perm, left, p - 1);
            }
            quicksort(v, &mut perm, p + 1, right);
        }
    }

    fn partition(v: &[i64], perm: &mut [usize], left: usize, right: usize) -> usize {
        let pivot = v[perm[right]];
        let mut i = left;

        for j in left..right {
            if v[perm[j]] < pivot {
                if i != j {
                    perm.swap(i, j);
                }
                i += 1;
            }
        }
        perm.swap(i, right);
        i
    }

    let mut perm = Vec::with_capacity(v.len());
    for i in 0..v.len() {
        perm.push(i);
    }

    quicksort(v, perm.as_mut_slice(), 0, v.len() - 1);
    perm
}

#[cfg(test)]
mod tests {
    use crate::argsort;
    use rand::Rng;

    #[test]
    fn sort_i64_vec() {
        let v = vec![9, 5, 3, 6, 4, 2, 7, 5];
        let p = argsort(v.as_slice());

        let mut sorted_v = v.clone();
        sorted_v.sort();

        for i in 0..v.len() {
            assert_eq!(sorted_v[i], v[p[i]]);
        }
    }

    #[test]
    fn sort_random_i64_vec() {
        let mut rng = rand::thread_rng();

        let n: usize = 100;
        let mut v = Vec::<i64>::with_capacity(n);
        for _ in 0..n {
            v.push(rng.gen());
        }

        let p = argsort(v.as_slice());
        for i in 0..n - 1 {
            assert!(v[p[i]] <= v[p[i + 1]]);
        }
    }
}
